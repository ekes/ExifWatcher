# Exif Watcher

Python script to remove, and optionally backup, and extract, EXIF data.

Operates on a directory, or directory tree.
Will run once, or watch the directories with inotify and change files as they are created.

## Requirements

 * Python - was written with Python 2, but probably runs in Python 3
 * [GExiv2](https://wiki.gnome.org/Projects/gexiv2) and [Watchdog](https://pypi.python.org/pypi/watchdog)
 
## Usage

 Type `ExifWatcher.py -h` for options help
 
### Once example

 `./ExifWatcher.py --once --backup --extract --recursive`
 
 Will recurse from the current directory, removing EXIF data from all (GExiv2) supported files. Making a backup of the file filename.ext.bak and an extract of the EXIF filename.ext.exif.csv
 
### Daemon example

 No full daemon control yet. So it just watches the directory, something to run in screen for example.
 
 `./ExifWatcher.py --daemon --backup --extract --recursive --directory=/data/incoming/watchdir --backup-directory=/data/backup --extract-directory=/data/exif/extract`
 
 Will recursively watch directory `/data/incoming/watchdir` when supported (GExiv2) files are created:
  * they will be backed up to /data/backup/path/as/in/watchdir/filename.ext.bak (never make this a directory in the watched tree, it'll loop)
  * they will have EXIF extracted to /data/exif/extract/path/as/in/watchdir/filename.ext.exif.csv
  * will have EXIF data removed from /data/incoming/watchdir/path/filename.ext