#!/usr/bin/env python

"""
  ExifWatcher.py

  Remove, and optionally backup, and extract EXIF data from files in a
  directory tree. Once, or watching the directories with inotify.

  todo: Gexiv2 seems to create a temporary file xx.jpg{\d}+.
    Could potentially, although when does this happen, it's always deleted
    before next process gets there. If not could create a loop, so this
    this should be ignored.
  todo: Proper daemon. Presently this is something to run in screen.
  todo: Sanity check: don't run in daemon mode if backup directory is in the
    watched directories. It'll loop.
"""

import argparse
import time  
import sys
import csv
import os
from shutil import copyfile
from gi.repository import GLib
from gi.repository import GExiv2
from watchdog.observers import Observer  
from watchdog.events import FileSystemEventHandler  

"""
  Backup file if option selected.

  We want to die if this fails.
"""
def backup_original(image_path, base_directory, backup_directory):
    original_path = os.path.normpath(os.path.join(base_directory, image_path))
    backup_path = os.path.normpath(os.path.join(backup_directory, image_path + '.bak'))
    # Check (sub-)directory for backup exists.
    check_directory(os.path.dirname(backup_path))
    if __debug__:
        print 'Backing up %(original)s to %(backup)s' % {'original': original_path, 'backup': backup_path}
    copyfile(image_path, backup_path)

"""
  Remove EXIF
"""
def remove_exif(image):
    if __debug__:
        print 'Removing EXIF and saving ' + image._path
    image.clear()
    image.save_file()

"""
  Save as EXIF as CSV
"""
def save_exif(image, image_path, extract_directory):
    csv_path = os.path.normpath(os.path.join(extract_directory, image_path + '.exif.csv'))
    # Check (sub-)directory for extracted CSV exists.
    check_directory(os.path.dirname(csv_path))
    if __debug__:
        print 'Writing EXIF to CSV ' + csv_path
    with open(csv_path, 'wb') as outfile:
      csv_writer = csv.writer(outfile)
      for tag in image.get_tags():
          csv_writer.writerow([tag, image.get(tag)])

"""
  Recursively load images from directories.
"""
def directory_images_recursive():
    loaded_images = {}
    for root, dirnames, filenames in os.walk('.'):
        print root
        print dirnames
        print filenames
        # todo dirnames to filename from root
        for filename in filenames:
            filepath = os.path.join(root, filename)
            image = load_image_metadata(filepath)
            if type(image) == GExiv2.Metadata:
                loaded_images[filepath] = image
    return loaded_images

"""
  Load any supported images metadata from a directory
"""
def directory_images():
    loaded_images = {}
    for filename in os.listdir('.'):
        image = load_image_metadata(filename)
        if type(image) == GExiv2.Metadata:
            loaded_images[filename] = image
    return loaded_images

"""
  Check directory exists, try and create if not.
"""
def check_directory(directory):
    if os.path.exists(directory):
	return

    try:
	os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
	    raise

"""
  Attempt to load an image metadata.
"""
def load_image_metadata(filepath):
    if __debug__:
        print 'Load image metadata: ' + filepath
    image = None
    try:
        image = GExiv2.Metadata(filepath)
    except GLib.Error as e:
        if e.code == 501:
            if __debug__:
                print "Unsupported type. File: " + filepath
            pass
        if e.code == 9:
            if __debug__:
                print "File gone away: " + filepath
            pass
        else:
            print "Unexpected error: " + str(e)
            raise
    return image

"""
  Watchdog event handler.
"""
class NewImageHandler(FileSystemEventHandler):

    def __init__(self, base_directory, extract_directory, backup_directory, extract, backup):
        if __debug__:
            print 'Initializing NewImageHandler base_directory: %(base_directory)s extract_directory %(extract_directory)s backup_directory %(backup_directory)s' % {'base_directory': base_directory, 'extract_directory': extract_directory, 'backup_directory': backup_directory}
        self.base_directory = base_directory
        self.extract_directory = extract_directory
        self.backup_directory = backup_directory
        self.extract = extract
        self.backup = backup

    def on_created(self, event):
        if event.is_directory:
            return

        image = load_image_metadata(event.src_path)
        if not image:
            return

        image_path = os.path.relpath(event.src_path, self.base_directory)
        if self.backup:
            backup_original(image_path, self.base_directory, self.backup_directory)
        if self.extract:
            save_exif(image, image_path, self.extract_directory)
        remove_exif(image)


def main():
    """
      Arguments
    """
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(description='Remove, and optionally backup and save, EXIF data.')
    parser.add_argument('--directory', help='Directory to operate on.', default='.')
    parser.add_argument('--recursive', help='If child directories should also be included', action="store_true")
    parser.add_argument('--backup', help='Store a backup copy of the image.', action="store_true")
    parser.add_argument('--backup-directory', help='Top level directory for backups. Default same directory as the image.', default=None)
    parser.add_argument('--extract', help='Store EXIF data in a CSV file.', action="store_true")
    parser.add_argument('--extract-directory', help='Top level directory for EXIF CSV. Default same directory as the image.', default=None)

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--once', help='Run once on all files, and stop.', action="store_true")
    group.add_argument('--daemon', help='Watch directory for new files.', action="store_true")

    args = parser.parse_args()

    if not args.daemon and not args.once:
        parser.print_help()
        sys.exit(2)

    # Base directory.
    base_directory = os.path.abspath(os.path.expanduser(args.directory))
    if not args.backup_directory:
        backup_directory = base_directory
    else:
        backup_directory = os.path.abspath(os.path.expanduser(args.backup_directory))
    if not args.extract_directory:
        extract_directory = base_directory
    else:
        extract_directory = os.path.abspath(os.path.expanduser(args.extract_directory))
    os.chdir(base_directory)

    """
      Daemon
    """
    if args.daemon:
        observer = Observer()
        observer.schedule(NewImageHandler(base_directory, extract_directory, backup_directory, args.extract, args.backup), base_directory, args.recursive)
        observer.start()
        # todo turn this into a daemon
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()

        observer.join()

    """
      Run once
    """
    if args.once:
        if args.recursive:
            matches = directory_images_recursive()
        else:
            matches = directory_images()
        
        if __debug__:
            print 'Matching files: ' + str(matches)
        
        # filepath is relative to base directory args.directory
        for filepath, image in matches.iteritems():
            if args.backup:
                backup_original(filepath, base_directory, backup_directory)
            if args.extract:
                save_exif(image, filepath, extract_directory)
            remove_exif(image)

if __name__ == '__main__':
    main()

